<?php
header('Content-type: text/html; charset=utf-8');
header("Access-Control-Allow-Origin: *");

define ( 'READFILE', true );

include_once("config.php");
include_once("lib.php");
include_once("database.php");
include_once("handlers.php");

$request = new NetRequest($_GET);
$method = $request->GetMethod();

switch ($method){
    case "generate":
		if(!$request->IsValidAuthKey()){
			exit("error=invalidauthkey");
		}
		GenerateHandler($request);
		break;
	case "login":
		LoginHandler($request);
		break;
	default:
		echo "error=not implemented method:".$method;
		break;
}
?>