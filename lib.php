<?php
if (!defined('READFILE')){
	exit("lah lah:)");
}

class NetResponse
{
	private $mas = array();
	
	public function AddParam($key, $val){
            $this->mas[$key] = urlencode($val);
	}
	
	public function AddError($val){
		$this->AddParam("error", $val);
	}
	
	public function SendResponse(){
		$res = "";
		foreach ($this->mas as $key => $val) {
			$res = $res.strtolower($key)."=".$val."&";
		}
                if(strlen($res) > 0)
                    echo substr($res,0,-1);
	}
        
	public function AddSendResponse(){
		if(count($this->mas) > 0){
			echo "&";
			$this->SendResponse();
		}
	}
}

class NetRequest{
	private $mas = array();
	
	function __construct($getMas){
		$this->mas = $getMas;
	}
	
	public function GetParam($key){
		if (array_key_exists($key, $this->mas)) {
			$val = urldecode($this->mas[$key]);
			$val = strip_tags($val);
			$val = mysql_real_escape_string($val);
			return $val;
		}else{
			return "";
		}
	}
    	
	public function IsValidAuthKey(){
		global $vk_id;
		global $vk_secret;
		global $isDebugMode;
		
		$auth_key = $this->GetParam("auth_key");
		
		if($isDebugMode)
			return true;
		
		return $auth_key == md5($vk_id."_".$this->GetParam("viewer_id")."_".$vk_secret);
	}
    
    public function GetMethod(){
		return $this->GetParam("method");
	}
	
	public function IsNumber($val){
		return filter_var($val, FILTER_VALIDATE_INT);
	}
}
?>