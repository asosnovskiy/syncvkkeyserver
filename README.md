# ОПИСАНИЕ #

Этот серверок нужен для выдачи одноразовых кодов, чтобы можно было синхронизировать auth_key.



## Как работает? ##

Все запросы и параметры передаются через GET. Ответ приходит в виде строки pName=pVal&p2Name=pVal2... Кодировка UTF-8.

Есть два запроса. Их имена передаются в параметре **method**.


### generate ###

Создает новый код доступа(code) для заданного пользователя. Пользователь должен быть авторизирован,т.е. передать **viewer_id** & **auth_key**. Код действует ограниченное время (по умолчанию, 60 сек). При каждом таком запросе код обновляется.


Параметры запроса: **viewer_id**, **auth_key**

Запрос вернет: **code** или **error**.


**Ошибки:**

error=**invalidviewerid** - неверный viewer_id

error=**invalidauthkey** - неверный auth_key


**Пример запроса GET:** method=generate&viewer_id=1&auth_key=7ny87h8mer


### login ###

Запрос на получение auth_key. Пользователь должен передать **viewer_id** & **code**, сгенерированный предыдущий запросом.


Параметры запроса: **viewer_id**, **code**

Запрос вернет: **auth_key** или **error**.


**Ошибки:**

error=**invalidviewerid** - неверный viewer_id

error=**invalidcode** - код неверный

error=**hack** - код выходит за пределы допустимых значений

error=**invalid** - код уже активирован

error=**featuretime** - срок начала действия ключа еще не начат

error=**codeexpired** - срок действия ключа истек

error=**noexistsaccount** - нет кода для заданного viewer_id


**Пример запроса GET:** method=login&viewer_id=1&code=1234567


## Настройка ##
1. качаем реп
2. создаем таблицу в бд MySQL
3. открываем файл **config.php** и вносим данные от базы, и от приложения ВК

## ВАЖНО ##

В релизе не забудьте переключить флаг isDebugMode = false для того, чтобы сверялся auth_key при генерации ключа!!!




### Пример кода для генерации кода на странице в ВК ###

```
#!javascript

var viewer_id = flashVars['viewer_id'];
var auth_key = flashVars['auth_key'];
    
$("#footer").html("ID: " + viewer_id + " CODE: ...");
 
$.ajax({
     url: "http://youdomain.com/myserver/engine.php",
     data: ({method: 'generate',viewer_id : viewer_id, auth_key : auth_key}),
     dataType: "text",
     success: function(msg){
			var code = msg.split("=",2)[1];
			$("#footer").html("ID: " + viewer_id + " CODE: " + code);
     }
});
```