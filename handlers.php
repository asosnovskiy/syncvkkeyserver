<?php
if (!defined('READFILE')){
	exit("lah lah:)");
}

//создать код
function GenerateHandler($request){
	global $db_table;
	
	$resp = new NetResponse;
	
	$viewer_id = $request->GetParam("viewer_id");
	
	//проверка пришедших данных
	if($request->IsNumber($viewer_id) == false){
		exit("error=invalidviewerid");
	}
	
	//пытаемся получить пользователя из базы
	$userAccount = DB::GetFetchArray("SELECT `code` FROM `".$db_table."` WHERE `viewer_id` =$viewer_id LIMIT 1");
	
	$newCode = rand(10000000,99999999);
	$newTime = time();
	//если пользователь есть - обновляем код
	if($userAccount){
		DB::SendQuery("UPDATE `".$db_table."` SET `time`=$newTime, `code`=$newCode, `valid`=1 WHERE `viewer_id`=$viewer_id");
	}else{
		DB::SendQuery("INSERT INTO `".$db_table."`(`viewer_id`,`code`,`time`,`valid`) value ($viewer_id,$newCode,$newTime,1)");
	}
	
	$resp->AddParam("code", $newCode);
	$resp->SendResponse();
}

//получить ключ
function LoginHandler($request){
	$viewer_id = $request->GetParam("viewer_id");
	$code = intval($request->GetParam("code"));
	
	//проверка данных
	if($request->IsNumber($viewer_id) == false){
		exit("error=invalidviewerid");
	}
	
	if($request->IsNumber($code) == false){
		exit("error=invalidcode");
	}
	
	if($code < 10000000 || $score > 99999999){
		exit("error=hack");
	}
	
	global $db_table;
	global $time_for_expired;
	
	$user_data = DB::GetFetchArray("SELECT `time`,`valid`,`code` FROM `".$db_table."` WHERE `viewer_id`=$viewer_id LIMIT 1");
	
	//if account exist
	if($user_data){
		$isValid = intval($user_data["valid"])."";
		$lastTime = intval($user_data["time"]);
		$userCode = intval($user_data["code"]);
		
		if($userCode != $code){
			DB::SendQuery("UPDATE `".$db_table."` SET `valid`=0 WHERE `viewer_id`=$viewer_id");
			exit("error=invalidcode");
		}
		
		if($isValid != "1"){
			exit("error=invalid");
		}
		
		$deltaTime = time() - $lastTime;
		
		//если мы в будущем)
		if($deltaTime < 0){
			exit("error=featuretime");
		}
		
		//код истек?
		if($deltaTime > $time_for_expired){
			exit("error=codeexpired");
		}
		
		//все ок - отдаем данные и обновляем запись
		global $vk_id;
		global $vk_secret;
		
		$authKey = md5($vk_id."_".$viewer_id."_".$vk_secret);
		
		DB::SendQuery("UPDATE `".$db_table."` SET `valid`=0 WHERE `viewer_id`=$viewer_id");
		
		$resp = new NetResponse;
		$resp->AddParam("auth_key", $authKey);
		$resp->SendResponse();
	}else{
		exit("error=noexistsaccount");
	}
}
?>